package httpsessionmanager

import (
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

// Session stores all the data of one user session
type Session struct {
	id         uint64
	lastAccess time.Time
	ticker     *time.Ticker
	data       map[string]interface{}
}

// Set stores data in the session using the given key
func (s *Session) Set(key string, data interface{}) {
	s.data[key] = data
}

// Get returns the data that was stored using the given key
// If the key isn't set, it returns nil
func (s *Session) Get(key string) interface{} {
	if v, ok := s.data[key]; ok {
		return v
	}
	return nil
}

// Exists checks if a value is assigned to the specified key
func (s *Session) Exists(key string) bool {
	_, ok := s.data[key]
	return ok
}

// Del deletes the map entry with the specified key and returns the deleted data
func (s *Session) Del(key string) interface{} {
	res := s.Get(key)
	delete(s.data, key)
	return res
}

// Map returns all the session data in a map
func (s *Session) Map() map[string]interface{} {
	return s.data
}

// Destroy deletes the user session and all of its data
func (s *Session) Destroy() {
	delete(sessions, s.id)
}

// SessionLifetime specifies when the sessions get garbage collected
// The lifetime of existing sessions don't get updated
var SessionLifetime = time.Duration(30 * time.Minute)

// sessions stores all the sessions
var sessions = map[uint64]*Session{}

// rnd is the random number generator used to generate new user IDs
var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))

// GetSession returns a session for the user specified by w and r
// It provides a new session for new users
func GetSession(w http.ResponseWriter, r *http.Request) *Session {
	cookie, err := r.Cookie("session")
	if err != nil {
		return newSession(w)
	}
	id, err := strconv.ParseUint(cookie.Value, 36, 64)
	if err != nil {
		return newSession(w)
	}
	if session, ok := sessions[id]; ok {
		session.lastAccess = time.Now()
		return session
	} else {
		return newSessionByID(w, id)
	}
}

// newSession creates a new session
func newSession(w http.ResponseWriter) *Session {
	return newSessionByID(w, newID())
}

// newSessionByID creates a new session with a specific id.
// It overrides the exsisting session assigned to the given id if there is one
func newSessionByID(w http.ResponseWriter, id uint64) *Session {
	http.SetCookie(w, &http.Cookie{
		Name:  "session",
		Value: strconv.FormatUint(id, 36),
	})
	sessions[id] = &Session{
		id,
		time.Now(),
		time.NewTicker(SessionLifetime),
		map[string]interface{}{},
	}
	// delete the session when its lifetime is over
	go func(s *Session) {
		exists := true
		for exists {
			<-s.ticker.C
			if time.Since(s.lastAccess) >= SessionLifetime {
				s.Destroy()
				exists = false
			}
		}
	}(sessions[id])

	return sessions[id]
}

// newID create a new unique user id to identify an user
func newID() uint64 {
	var id uint64
	for id = rnd.Uint64(); idAlreadyInUse(id); id = rnd.Uint64() {

	}
	return id
}

// idAlreadyInUse checks if the given id is already assigned to an user
func idAlreadyInUse(id uint64) bool {
	_, res := sessions[id]
	return res
}
