package httpsessionmanager

/*

Package httpsessionmanager implements a HTTP session to identify users and to store data for them individually.
Sessions don't get stored permanently. The default session age is 30 minutes.

*/
