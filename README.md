# httpsessionmanager

httpsessionmanager is a small Go package that provides HTTP sessions to identify users (like `$_SESSION` in PHP).
You can securely store data in the sessions without worrying that the users can read it.

**Note that this repository is under private development for now. It will be published when it's finished.**

## Installation

*COMING SOON*

## Usage Examples

Set up a HTTP server in Go like this:

```
package main

import (
    "fmt"
    "net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "hello world")
}

func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}
```

To use the sessions, you have to import httpsessionmanager and use the `GetSession()` method to get a session.
The session provides a few important methods:

- Store data in the session: `Set(key string, data interface{})`

- Get stored data from the session: `Get(key string) interface{}`

- Del deletes (and returns) the data associated with the specified key: `Del(key string) interface{}`

- Exists checks if there is a value assigned to the given key: `Exists(key string) bool`

- Map provides a `map[string]interface{}` with all the data stored in the session: `Map() map[string]interface{}`

- Destroy deletes the whole session with all its data: `Destroy()`

Here is an exmaple:

```
package main

import (
	"fmt"
	"net/http"
	"httpsessionmanager"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	// Get the existing session for the current user (or create a new one if there isn't one)
	session := httpsessionmanager.GetSession()
	session.Set("message", "hello world")
	http.Redirect(w, r, "/output", 303)
}

func outputHandler(w http.ResponseWriter, r *http.Request) {
	// Get the existing session for the current user (or create a new one if there isn't one)
	session := httpsessionmanager.GetSession()
	// send "hello world" to the user
	fmt.Fprintln(session.Get("message"))
}

func main() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/message", outputHandler)
	http.ListenAndServe(":8080", nil)
}
```

**Sessions have a life time. They get garbage collected if their life time is over**

The default life time is 30 minutes.

You can change the lifetime of new sessions to 5 minutes like this: `httpsessionmanager.SessionLifetime = time.Duration(5 * time.Minute)`