package httpsessionmanager

import (
	"fmt"
	"log"
	"net/http"
	"testing"
)

// TestSession needs HTTP requests from the user
func TestSession(t *testing.T) {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	session := GetSession(w, r)
	log.Print(sessions)
	log.Print(session)
	session.Set("testvar", "testval")
	fmt.Fprint(w, "hello world")
}
